#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <netdb.h> 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#define TAM 256
#define LIMIT 50
#define TRANSFER_SIZE 1500

clock_t start, end;
double cpu_time_used;
struct hostent * server;
struct sockaddr_in serv_addr, serv_addr_udp;
char * pch, usuario[LIMIT], host[LIMIT], puerto[LIMIT], password[LIMIT], buffer[TAM], login[TAM];
int sockfd, sockfd_udp, port, n, i, intentos = 0, max_intentos = 3, authorized = 0, puerto_udp = 6021, tamano_direccion;

/* Rutina de start scanning */
void startScannning(int socket) {

  int recv_size = 0, size = 0, read_size, write_size, packet_index = 1, stat, buffer_fd;
  char imagearray[TRANSFER_SIZE], buffer[] = "Confirmacion";
  FILE * image;
  struct timeval timeout = { 10, 0 };
  fd_set fds;

  /* Recibo el tamano de la imagen */
  do {
    stat = read(socket, & size, sizeof(int));
  } while (stat < 0);

  printf("Tamano total de la imagen a recibir: %i bytes \n", size);

  /* Envio senal de confirmacion */
  do {
    stat = write(socket, & buffer, sizeof(int));
  } while (stat < 0);

  /* Intento abrir imagen de destino */
  image = fopen("Captura2.JPG", "w");
  if (image == NULL) {
    printf("Error abriendo archivo de imagen \n");
    exit(EXIT_FAILURE);
  }

  /* Bucle mientras a�n no recibo el archivo completo */
  while (recv_size < size) {

    FD_ZERO( & fds);
    FD_SET(socket, & fds);

    buffer_fd = select(FD_SETSIZE, & fds, NULL, NULL, & timeout);

    if (buffer_fd < 0)
      printf("Error: bad file descriptor set.\n");

    if (buffer_fd == 0)
      printf("Error: buffer read timeout expired.\n");

    if (buffer_fd > 0) {
      do {
        read_size = read(socket, imagearray, TRANSFER_SIZE);
      } while (read_size < 0);

      printf("Numero de paquete recibido: %i \n", packet_index);
      printf("Tamano del paquete recibido: %i bytes \n", read_size);

      /* Escribo los datos leidos en archivo de imagen */
      write_size = fwrite(imagearray, 1, read_size, image);

      if (read_size != write_size) {
        printf("Error en lectura/escritura \n");
      }

      /* Increment the total number of bytes read */
      recv_size += read_size;
      packet_index++;
      printf("Tamano total de la imagen recibida: %i bytes \n", recv_size);
    }
  }

  fclose(image); /* Cierro archivo de imagen */
  end = clock(); /* Finalizo la cuenta de tiempo */
  cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC; /* Calculo el tiempo que demoro la transferencia en segundos */

  printf("Imagen recibida completamente en %f segundos. \n", cpu_time_used); /* Imprimo el tiempo */

}

/* Rutina de obtener telemetria */
void obtenerTelemetria() {

  int salir = 0;

  /* Mando saludo inicial al servidor */
  sendto(sockfd_udp, "hola", TAM, 0, (struct sockaddr * ) NULL, sizeof(serv_addr_udp));

  while (salir == 0) {
    recvfrom(sockfd_udp, buffer, sizeof(buffer), 0, (struct sockaddr * ) NULL, NULL);
    if (!strcmp("fin funcion", buffer))
      salir = 1;
    else
      puts(buffer);
  }

}

/* Rutina de update firmware */
void updateFirmware(int socket) {
  
  FILE * firmware;
  int size, read_size, stat, packet_index = 1;
  char send_buffer[TRANSFER_SIZE];

  /* Intento abrir firmware */
  firmware = fopen("firmware.bin", "rb");
  if (firmware == NULL) {
    printf("Error abriendo archivo de firmware \n");
    exit(EXIT_FAILURE);
  }

  /* Obtengo el tamano del firmware */
  fseek(firmware, 0, SEEK_END);
  size = ftell(firmware);
  fseek(firmware, 0, SEEK_SET);
  printf("Tamano total del firmware a enviar: %i bytes \n", size);

  /* Envio el tamano del firmware */
  write(socket, (void * ) & size, sizeof(int));

  /* Espero confirmacion de cliente */
  do {
    stat = read(socket, buffer, TAM - 1);
  } while (stat < 0);

  /* Enviar firmware como array de Bytes */
  printf("Enviando paquete de firmware al servidor... \n");

  while (!feof(firmware)) {
    /* Coloco en el buffer de envio informacion del firmware */
    read_size = fread(send_buffer, 1, sizeof(send_buffer), firmware);

    /* Envio informacion a traves del socket */
    do {
      stat = write(socket, send_buffer, read_size);
    } while (stat < 0);

    /* Incremento el numero de paquete */
    packet_index++;

    /* Cero el buffer de envio */
    bzero(send_buffer, sizeof(send_buffer));
  }

  printf("Firmware enviado correctamente. Por favor espere mientras se realiza la actualizacion del servidor... \n");
  sleep(30);
  printf("Actualizacion concluida, por favor inicie sesion nuevamente \n");
  exit(EXIT_SUCCESS);

}

/* Funcion que realiza la autenticacion del usuario y cuenta la cantidad de intentos fallidos */
void autenticar() {

  /* Leo password por consola. */
  printf("Password: ");
  fgets(password, LIMIT, stdin);
  password[strlen(password) - 1] = '\0';

  /* Armo la cadena "user;pass" a transmitir */
  strcpy(login, usuario);
  strcat(login, ";");
  strcat(login, password);

  /* Transmito la cadena y leo respuesta */
  n = write(sockfd, login, strlen(login));
  memset(buffer, '\0', TAM);
  n = read(sockfd, buffer, TAM);

  /* Imprimo la respuesta por pantalla */
  printf("Respuesta: %s\n", buffer);

  /* Verifico si la clave es correcta, basado en la respuesta */
  if (strcmp("Nombre de usuario y/o contrasena incorrecto", buffer) == 0)
    intentos++;
  else
    authorized = 1;
}

int main(int argc, char * argv[]) {

  /* Valido que se ingresen al menos 3 argumentos. */
  if (argc < 3) {
    fprintf(stderr, "Uso %s connect usuario@numero_ip:port \n", argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Valido que el segundo argumento sea connect, ya que es la unica opcion valida. */
  if (strcmp("connect", argv[1]) != 0) {
    fprintf(stderr, "Argumento %s no valido \n", argv[1]);
    exit(EXIT_FAILURE);
  }

  /* Obtengo Usuario, Host y Puerto, separando tokens del tercer Argumento. */
  pch = strtok(argv[2], ":@");

  for (i = 0; i < 3; i++) {
    if (i == 0)
      strcpy(usuario, pch);
    if (i == 1)
      strcpy(host, pch);
    if (i == 2)
      strcpy(puerto, pch);

    pch = strtok(NULL, ":@");
  }

  /* Convierto a entero el string puerto. */
  port = atoi(puerto);

  /* Pruebo obtener File Descriptor para socket TCP. */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("Error abriendo socket TCP \n");
    exit(EXIT_FAILURE);
  }

  /* Verifico que el host sea valido. */
  server = gethostbyname(host);
  if (server == NULL) {
    fprintf(stderr, "Error, el host no existe \n");
    exit(EXIT_FAILURE);
  }

  /* Alloco memoria y seteo valores para la estructura de datos TCP */
  memset((char * ) & serv_addr, '0', sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  bcopy((char * ) server -> h_addr, (char * ) & serv_addr.sin_addr.s_addr,
    server -> h_length);
  serv_addr.sin_port = htons(port);
  
  /* Intento abrir conexion al socket TCP destino. */
  if (connect(sockfd, (struct sockaddr * ) & serv_addr, sizeof(serv_addr)) < 0) {
    perror("Error estableciendo conexion TCP \n");
    exit(EXIT_FAILURE);
  }

  /* Alloco memoria y seteo valores para la estructura de datos UDP */
  serv_addr_udp.sin_family = AF_INET;
  bcopy((char * ) server -> h_addr, (char * ) & serv_addr_udp.sin_addr.s_addr, server -> h_length);
  serv_addr_udp.sin_port = htons(puerto_udp);

  /* Pruebo obtener File Descriptor para socket UDP. */
  if ((sockfd_udp = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("Error abriendo socket UDP \n");
    exit(EXIT_FAILURE);
  }
  
  /* Intento abrir conexion al socket UDP destino. */
  if (connect(sockfd_udp, (struct sockaddr * ) & serv_addr_udp, sizeof(serv_addr_udp)) < 0) {
    perror("Error estableciendo conexion UDP \n");
    exit(EXIT_FAILURE);
  }

  while (authorized == 0) {
    /* Si logre conexion, debo autenticar user y password. */
    autenticar();

    /* Verifico si alcanzo maxima cantidad de intentos */
    if (intentos == max_intentos) {
      printf("Se alcanzo el maximo de intentos permitidos, desconectando.. \n");
      exit(EXIT_FAILURE);
    }
  }

  /* Leo comandos y escribo salidas. */
  while (1) {
    printf("%s@%s:~$ ", usuario, host);
    memset(buffer, '\0', TAM);
    fgets(buffer, TAM - 1, stdin);
	
	/* Envio por socket TCP el comando ingresado */ 
    n = write(sockfd, buffer, strlen(buffer));
    if (n < 0) {
      perror("Error escribiendo en socket TCP \n");
      exit(EXIT_FAILURE);
    }

    /* Limpio salto de linea del buffer */
    buffer[strlen(buffer) - 1] = '\0';

    /* Verifico si escribio update firmware.bin */
    if (!strcmp("update firmware.bin", buffer)) {
      updateFirmware(sockfd);
    }

    /* Verifico si escribio start scanning */
    else if (!strcmp("start scanning", buffer)) {
      start = clock(); /* Inicio conteo de tiempo */
      startScannning(sockfd); 
    }

    /* Verifico si escribio obtener telemetria */
    else if (!strcmp("obtener telemetria", buffer)) {
      obtenerTelemetria();
    }

	/* Verifico si escribio exit */
    else if (!strcmp("exit", buffer)) {
      exit(EXIT_SUCCESS);
    }	else {
		
	  /* Cero el buffer de recepcion y leo comando */
      memset(buffer, '\0', TAM);
      n = read(sockfd, buffer, TAM);
      if (n < 0) {
        perror("Error leyendo en socket TCP \n");
        exit(EXIT_FAILURE);
      }
	  
      printf("Respuesta: %s \n", buffer);
    }
  }
  return 0;
}