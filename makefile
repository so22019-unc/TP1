CC = gcc
CFLAGS = -Werror -Wall -pedantic -Wextra -Wunused -D _GNU_SOURCE

default: all

all: cliente servidor

cliente: cliente.c
	$(CC) $(CFLAGS) -o cliente cliente.c
	
servidor: servidor.c
	$(CC) $(CFLAGS) -o servidor servidor.c

clean:
	rm -rf cliente servidor
