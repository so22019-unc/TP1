#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/reboot.h>
#include <netinet/in.h>
#define TAM 256
#define LIMIT 50
#define TRANSFER_SIZE 1500
#define SOFT_VERSION "3.4.1"

socklen_t clilen, clilen_udp;
struct sockaddr_in serv_addr, cli_addr, serv_addr_udp, cli_addr_udp;
char buffer[TAM], login[TAM], usuario[LIMIT] =  "christian", password[LIMIT] = "SisOp2";
int pid, sockfd, newsockfd, sockfd_udp, n, puerto_tcp = 6020, puerto_udp = 6021, authenticated = 0;

/* Rutina de start scanning */
void startScannning(int socket) {

  FILE * picture;
  int size, read_size, stat, packet_index = 1;
  char send_buffer[TRANSFER_SIZE];

  /* Intento abrir imagen de origen*/
  picture = fopen("Captura.JPG", "r");
  if (picture == NULL) {
    printf("Proceso: %d. Error abriendo archivo de imagen \n", getpid());
    exit(EXIT_FAILURE);
  }

  /* Obtengo el tamano de la imagen */
  fseek(picture, 0, SEEK_END);
  size = ftell(picture);
  fseek(picture, 0, SEEK_SET);
  printf("Proceso: %d. Tamano total de la imagen a enviar: %i bytes \n", getpid(), size);

  /* Envio el tamano de la imagen */
  printf("Proceso: %d. Transmitiendo el tamano de la imagen... \n", getpid());
  write(socket, (void * ) & size, sizeof(int));

  /* Espero confirmacion de cliente */
  do {
    stat = read(socket, buffer, TAM - 1);
  } while (stat < 0);

  /* Enviar imagen como array de Bytes */
  printf("Proceso: %d. Recibi confirmacion, empiezo la transferencia de imagen como array de Bytes... \n", getpid());

  while (!feof(picture)) {
    /* Coloco en el buffer de envio informacion de la imagen */
    read_size = fread(send_buffer, 1, sizeof(send_buffer), picture);

    /* Envio informacion a traves del socket */
    do {
      stat = write(socket, send_buffer, read_size);
    } while (stat < 0);

    printf("Proceso: %d. Numero de paquete enviado: %i \n", getpid(), packet_index);
    printf("Proceso: %d. Tamano del paquete enviado: %i bytes \n", getpid(), read_size);

    /* Incremento el numero de paquete */
    packet_index++;

    /* Cero el buffer de envio */
    bzero(send_buffer, sizeof(send_buffer));
  }

  printf("Proceso: %d. Imagen enviada completamente. \n", getpid());
}

/* Rutina de update firmware */
void updateFirmware(int socket) {
  int recv_size = 0, size = 0, read_size, write_size, packet_index = 1, stat, buffer_fd, perm = 0755;
  char imagearray[TRANSFER_SIZE], buffer[] = "Confirmacion";
  FILE * firmware;
  struct timeval timeout = { 10, 0 };
  fd_set fds;
  
  /* Renombro el ejecutable actual para tener backup */
  n = rename("servidor", "servidor_previus_release");
  if (n == 0) {
    printf("Proceso: %d. Ejecutable actual renombrado \n", getpid());
  } else {
    printf("Proceso: %d. Error renombrando ejecutable \n", getpid());
  }

  /* Recibo el tamano del firmware */
  do {
    stat = read(socket, & size, sizeof(int));
  } while (stat < 0);

  printf("Proceso: %d. Tamano total del firmware a recibir: %i bytes \n", getpid(), size);

  /* Envio senal de confirmacion */
  do {
    stat = write(socket, & buffer, sizeof(int));
  } while (stat < 0);

  /* Intento abrir archivo de destino */
  firmware = fopen("servidor", "wb");
  if (firmware == NULL) {
    perror("Error abriendo archivo de firmware \n");
    exit(EXIT_FAILURE);
  }
  
  /* Seteo permisos de ejecucion en archivo creado */
  fchmod(fileno(firmware),perm);

  /* Bucle mientras a�n no recibo el archivo completo */
  while (recv_size < size) {

    FD_ZERO( & fds);
    FD_SET(socket, & fds);

    buffer_fd = select(FD_SETSIZE, & fds, NULL, NULL, & timeout);

    if (buffer_fd < 0)
      perror("Error: bad file descriptor set.\n");

    if (buffer_fd == 0)
      perror("Error: buffer read timeout expired.\n");

    if (buffer_fd > 0) {
      do {
        read_size = read(socket, imagearray, TRANSFER_SIZE);
      } while (read_size < 0);

      printf("Proceso: %d. Numero de paquete recibido: %i \n", getpid(), packet_index);
      printf("Proceso: %d. Tamano del paquete recibido: %i bytes \n", getpid(), read_size);

      /* Escribo los datos leidos en archivo de firmware */
      if ((write_size = fwrite(imagearray, 1, read_size, firmware)) < 0) {
        perror("fwrite");
        exit(EXIT_FAILURE);
      }
      fseek(firmware, 0, SEEK_END);

      if (read_size != write_size) {
        perror("Error en lectura/escritura \n");
      }

      /* Incremento numero de paquetes y tamano total recibido */
      recv_size += read_size;
      packet_index++;
      printf("Proceso: %d. Tamano total del firmware recibido: %i bytes \n", getpid(), recv_size);
    }
  }

  printf("Proceso: %d. Firmware actualizado correctamente, reiniciando el sistema \n", getpid());

  sync();
  reboot(RB_AUTOBOOT);

}

/* Rutina para obtener telemetria*/
void obtenerTelemetria() {

  char hostname[1024], * bla;
  struct sysinfo info;
  float f_load = 1.f / (1 << SI_LOAD_SHIFT);

  sysinfo( & info);
  gethostname(hostname, 1024);
  
  /* Recibo saludo inicial del cliente */
  n = recvfrom(sockfd_udp, buffer, sizeof(buffer),
    0, (struct sockaddr * ) & cli_addr_udp, & clilen_udp);
  buffer[n] = '\0';

  /* Envio informacion de telemetria */
  asprintf( & bla, "Satelite ID: %1s", hostname);
  sendto(sockfd_udp, bla, TAM, 0, (struct sockaddr * ) & cli_addr_udp, sizeof(cli_addr_udp));
  asprintf( & bla, "Uptime: %02ld horas, %02ld minutos, %02ld segundos", info.uptime / 3600, info.uptime % 3600 / 60, info.uptime % 60);
  sendto(sockfd_udp, bla, TAM, 0, (struct sockaddr * ) & cli_addr_udp, sizeof(cli_addr_udp));
  asprintf( & bla, "Version de software: %1s", SOFT_VERSION);
  sendto(sockfd_udp, bla, TAM, 0, (struct sockaddr * ) & cli_addr_udp, sizeof(cli_addr_udp));
  asprintf( & bla, "Free RAM: %lu MB - Shared RAM: %lu MB - Buffer RAM: %lu MB - Total RAM %lu MB", info.freeram / 1024 / 1024, info.sharedram / 1024 / 1024, info.bufferram / 1024 / 1024, info.totalram / 1024 / 1024);
  sendto(sockfd_udp, bla, TAM, 0, (struct sockaddr * ) & cli_addr_udp, sizeof(cli_addr_udp));
  asprintf( & bla, "Cantidad de procesos: %d", info.procs);
  sendto(sockfd_udp, bla, TAM, 0, (struct sockaddr * ) & cli_addr_udp, sizeof(cli_addr_udp));
  asprintf( & bla, "Carga promedio (1 min): %.2f (%.0f%% CPU)", info.loads[0] * f_load, info.loads[0] * f_load * 100 / get_nprocs());
  sendto(sockfd_udp, bla, TAM, 0, (struct sockaddr * ) & cli_addr_udp, sizeof(cli_addr_udp));
  asprintf( & bla, "fin funcion");
  sendto(sockfd_udp, bla, TAM, 0, (struct sockaddr * ) & cli_addr_udp, sizeof(cli_addr_udp));

}

int main() {

  /* Pruebo obtener File Descriptor para socket TCP */
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    perror("Error abriendo socket TCP \n");
    exit(EXIT_FAILURE);
  }

  /* Pruebo obtener File Descriptor para socket UDP */
  if ((sockfd_udp = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("Error abriendo socket UDP \n");
    exit(EXIT_FAILURE);
  }

  /* Seteo valores para la estructura de datos TCP */
  memset( & serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(puerto_tcp);

  /* Seteo valores para la estructura de datos UDP */
  memset( & serv_addr_udp, 0, sizeof(serv_addr_udp));
  memset( & cli_addr_udp, 0, sizeof(cli_addr_udp));
  serv_addr_udp.sin_family = AF_INET;
  serv_addr_udp.sin_addr.s_addr = INADDR_ANY;
  serv_addr_udp.sin_port = htons(puerto_udp);

  /* Hago el bind del socket TCP*/
  if (bind(sockfd, (struct sockaddr * ) & serv_addr, sizeof(serv_addr)) < 0) {
    perror("Error haciendo bind TCP \n");
    exit(EXIT_FAILURE);
  }

  /* Hago el bind del socket UDP*/
  if (bind(sockfd_udp, (struct sockaddr * ) & serv_addr_udp, sizeof(serv_addr_udp)) < 0) {
    perror("Error haciendo bind UDP \n");
    exit(EXIT_FAILURE);
  }

  /* Imprimo PID y puerto */
  printf("Proceso: %d. Socket TCP disponible: %d \n", getpid(),
    ntohs(serv_addr.sin_port));
  printf("Proceso: %d. Socket UDP disponible: %d \n", getpid(),
    ntohs(serv_addr_udp.sin_port));

  listen(sockfd, 5);
  clilen = sizeof(cli_addr);
  clilen_udp = sizeof(cli_addr_udp);

  /* Main Loop del Programa */
  while (1) {

    /* Acepto conexiones TCP nuevas */
    newsockfd = accept(sockfd, (struct sockaddr * ) & cli_addr, & clilen);
    if (newsockfd < 0) {
      perror("Error aceptando conexion TCP \n");
      exit(EXIT_FAILURE);
    }

    /* Hago fork para atender la nueva conexion en un subproceso */
    pid = fork();
    if (pid < 0) {
      perror("Error haciendo fork \n");
      exit(EXIT_FAILURE);
    }

    if (pid == 0) {
      /* Proceso hijo */
      close(sockfd);

      /* Loop para enviar y recibir comandos */
      while (1) {

        /* Si no esta autenticado, valido usuario y contrasena */
        if (authenticated == 0) {

		  /* Armo cadena de autenticacion para comparar */
          strcpy(login, usuario);
          strcat(login, ";");
          strcat(login, password);

          /* Cero el buffer de recepcion y leo credenciales */
          memset(buffer, 0, TAM);
          n = read(newsockfd, buffer, TAM - 1);
          if (n < 0) {
            perror("Error leyendo en socket TCP \n");
            exit(EXIT_FAILURE);
          }
		  
		  /* Valido que las credenciales sean correctas */ 
          if (strcmp(login, buffer) != 0) {
            n = write(newsockfd, "Nombre de usuario y/o contrasena incorrecto", 43);
            if (n < 0) {
              perror("Error escribiendo en socket TCP \n");
              exit(EXIT_FAILURE);
            }
			
          } else {
            printf("SERVIDOR: Nuevo cliente, que atiende el proceso hijo: %d\n", getpid());

            n = write(newsockfd, "Acceso autorizado", 17);
            if (n < 0) {
              perror("Error escribiendo en socket TCP \n");
              exit(EXIT_FAILURE);
            }

            authenticated = 1;
          }
        }

        /* Si esta autenticado, interpreto comando */
        else {

          /* Cero el buffer de recepcion y leo comando */
          memset(buffer, 0, TAM);
          n = read(newsockfd, buffer, TAM - 1);
          if (n < 0) {
            perror("Error leyendo en socket TCP \n");
            exit(EXIT_FAILURE);
          }
		  
		  /* Logueo en consola lo recibido */
          printf("Proceso: %d. Recibi: %s", getpid(), buffer);

          /* Limpio salto de linea del buffer */
          buffer[strlen(buffer) - 1] = '\0';

          /* Verifico si mando update firmware.bin */
          if (!strcmp("update firmware.bin", buffer)) {
            updateFirmware(newsockfd);
          }

          /* Verifico si mando start scanning */
          else if (!strcmp("start scanning", buffer)) {
            startScannning(newsockfd);
          }

          /* Verifico si mando obtener telemetria */
          else if (!strcmp("obtener telemetria", buffer)) {
            obtenerTelemetria();
          }

          /* Verifico si mando exit */
          else if (!strcmp("exit", buffer)) {
            exit(EXIT_SUCCESS); /* Mato el subproceso que estaba ejecutandose */
          }

          /* Se trata de un comando no valido o vacio */
          else if (strcmp("", buffer) || !strcmp("", buffer)) {
            n = write(newsockfd, "Comando invalido, consulte la documentacion.", 44);
            if (n < 0) {
              perror("Error escribiendo en socket TCP \n");
              exit(EXIT_FAILURE);
            }
          }
        }
      }
    } else {
      close(newsockfd);
    }
  }
  return 0;
}
